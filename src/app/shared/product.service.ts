import { Injectable } from '@angular/core';
import{HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import { Observable } from 'rxjs';
import { product } from './model';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  
  constructor(private httpClient:HttpClient) { 

  }

  //return Observable<Object>
  // getProduct(){
  //   const apiUrl='https://steelsoftware.azurewebsites.net/api/FresherFPT';
  //   return this.httpClient.get(apiUrl);
  // }

  getProducts():Observable<HttpResponse<product[]>>{
    const apiUrl='https://steelsoftware.azurewebsites.net/api/FresherFPT';
    
    const options={
      headers:new HttpHeaders({
        'Content-Type':'application/json'
      }),
      observe:'response' as const
    }
    
    return this.httpClient.get<product[]>(apiUrl,options);
  }

  // deleteProduct(id:string){
  //   const apiUrl=`https://steelsoftware.azurewebsites.net/api/FresherFPT/${id}`;
  //   return this.httpClient.delete('apiUrl', {observe:'response'});
  // }
}
