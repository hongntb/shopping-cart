import { Component, OnInit } from '@angular/core';
import { product } from './shared/model';
import { ProductService } from './shared/product.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  products:any;
  showShoppingCart = false;
  productAdded: product[] = [];

  constructor(private productService:ProductService){
  }

  ngOnInit():void{
    this.getProducts();
  }

  getProducts(){
    this.productService.getProducts().subscribe(res=>{
      console.log(res);
      if(res.status==200){
        this.products=res.body;
      }
      else{
        alert('Get data from server failed');
      }
    });
  }

  // deleteProduct(id:string){
  //   console.log(id);
  //    this.productService.deleteProduct(id).subscribe(res=>{
  //      if(res.status==200){
  //        alert('Delete successfully');
  //       //  this.getProducts();
  //      }
  //      else{
  //        alert('Delete failed');
  //      }
  //    });
  // }

  addToCart(product: any) {
    var obj: product = {
      id: product.id,
      productName:product.productName,
      price: product.price,
      promotionPrice: product.promotionPrice,
      quantity: 1,
      image: product.image,
    };
    if (this.productAdded.length == 0) {
      this.productAdded.push(obj);
    } else {
      var existed = this.productAdded.find((x) => x.id == product.id);

      if (existed) {
        existed.quantity += 1;
      } else {
        this.productAdded.push(obj);
      }
    }
    alert("Thêm sản phẩm vào giỏ hàng thành công");
    console.log(JSON.stringify(this.productAdded));
  }
  
  dec(item: product) {
    if (item.quantity > 0) {
      item.quantity -= 1;
    }
  }
}
